<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	//return View::make('hello');
	return Redirect::to(Lang::code(), 302);
});

Route::get('/dashboard/user', function()
{
	return Redirect::to(Lang::code() . '/dashboard/user', 302);
});

Route::get('/dashboard/admin', function()
{
	//return View::make('hello');
	return Redirect::to(Lang::code() . '/dashboard/admin', 302);
});

Route::langGroup(function() {
    Route::get('/', function(){
        return View::make('hello');
    });

    Route::get('/test', array(
    	'uses' => 'TestController@index'
    ));

    /**
	 *	Backend Routes
     */
    Route::group(array('prefix' => 'dashboard'), function() {
    	Route::group(array('prefix' => 'user'), function() {
    		Route::get('/', function() {
    			return 'User dashboard';
    		});
    	});

    	Route::group(array('prefix' => 'admin'), function() {
    		Route::get('/', function() {
    			return 'Admin dashboard';
    		});
    	});
    });
});
