<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contents', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->string('type', 50);
		    $table->integer('category_id');
		    $table->integer('author_id');
		    $table->tinyInteger('is_public');
		    $table->tinyInteger('is_deleted');
		    $table->string('title_for_slug');
		    $table->string('slug');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contents');
	}

}
