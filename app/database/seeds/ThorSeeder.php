<?php

    class ThorSeeder extends \Seeder
    {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            Thor\Language\Language::create(array('name' => 'Bahasa Indonesia', 'code' => 'id', 'locale' => 'id_ID', 'is_active' => true, 'sorting' => 6));
            Thor\Language\Language::create(array('name' => 'Arabic', 'code' => 'ar', 'locale' => 'ar_AR', 'is_active' => true, 'sorting' => 7));
        }

    }
