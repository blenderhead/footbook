<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTranslations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('content_translations', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->integer('content_id')->unsigned();
		    $table->string('title');
		    $table->text('content');
		    $table->string('locale');
		    $table->timestamps();
		    $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('content_translations');
	}

}
