<?php
	
	use Cviebrock\EloquentSluggable\SluggableInterface;
	use Cviebrock\EloquentSluggable\SluggableTrait;

	class Content extends Eloquent implements SluggableInterface
	{
		use \Dimsav\Translatable\Translatable;
		use SluggableTrait;

    	public $translatedAttributes = array('title', 'content');

    	protected $sluggable = array(
	        'build_from' => 'title_for_slug',
	        'save_to'    => 'slug',
	    );

	    public function attachment()
	    {
	        return $this->hasOne('Attachment', 'id', 'content_id');
	    }
	}