<?php

	class TestController extends BaseController
	{	
		public function index()
		{
			// save content
			$test = new Content();
			$test->type = 'news';
			$test->category_id = 1;
			$test->author_id = 100;
			$test->is_public = 1;
			$test->is_deleted = 1;
			$test->title_for_slug = 'Ini judul kontent';
			$test->translate('id')->title = 'Judul';
			$test->translate('id')->content = 'Isi Konten';
			$test->translate('en')->title = 'Title';
			$test->translate('en')->content = 'Content';
			$test->save();
			//var_dump($test->id);

			// save attachment
			$att = new Attachment();
			$att->type = 'content';
			$att->content_id = $test->id;
			$att->author_id = $test->author_id;
			$att->filename = 'filename.jpg';
			$content = Content::find($test->id);
			$attachment = $content->attachment()->save($att);

			//$content = Content::find(3);
  			//echo $content->translate('id')->title; // Greece
		}
	}