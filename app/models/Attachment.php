<?php

	class Attachment extends Eloquent
	{
		public function content()
    	{
        	return $this->belongsTo('Content', 'content_id', 'id');
    	}
	}